BASHtop Automated Installation Script by Papas Films                                                v1.999

---

![BASHtop Automated Installation Script by Papas Films](Images/BAIS.png)

---

<u>**Known Issues**</u>

1. There seems to be a problem with the clonning of the bashtop github project (**ONLY IN THE TAILS VERSION**). You will have to install it manually. The installation steps have been updated to help you with doing so. If you have allready installed bashtop then ignore the warnings.

**We are currently trying to fix the above issues. An ETA is not yet possible to be made**

---

<u>**To install:**</u>

**<u>Linux:</u>**

1. **<u>Manual installation</u>**

Move the installed BASHtop script (not included) to `/usr/local/bin` and save it as bashtop

Then, move the `bashtop.ico` file to the same location

The last step is to move the `bashtop.desktop` file to `/usr/share/applications`

That's it! Now, restart your DE (`alt + F2` and then type `r` if you are in GNOME). You can also reboot your whole system.

2. **<u>Automated installation</u>**

Type the following commands to your terminal emulator:

`cd`

`git clone https://gitlab.com/PapasFilms/bashtop-application.desktop-file-with-icon.git`

`cd /home/$USER/bashtop-application.desktop-file-with-icon`

`sudo chmod +x bashtop-auto-config.sh`

`./bashtop-auto-config.sh`

Done!

<u>**Tails:**</u>

Type the following commands on your terminal emulator:

**IMPORTANT:** You will need to have `make` installed and added to the persistent auto installation list.

`cd`

`git clone https://gitlab.com/PapasFilms/bashtop-application.desktop-file-with-icon.git`

`git clone https://github.com/aristocratos/bashtop.git`

`cd bashtop`

`sudo make install`

`cd`

`cd bashtop-application.desktop-file-with-icon`

`sudo chmod +x bashtop-tails-auto-config.sh`

`./bashtop-tails-auto-config.sh`

Done!

We recommend you keeping the clonned repository inside your persistent folder.

<u>**Usage:**</u>

Just search your appdrawer for bashtop. The program will launch in your default terminal emulator.

**<u>Reason behind making this:</u>**

I always found it hard to make new `application.desktop` files and I want to make this proccess a litle bit easier for popular scripts.

**<u>Credits:</u>**

BASHtop: aristocratos

BASHtop icon: ME :)
