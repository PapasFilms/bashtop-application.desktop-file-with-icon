echo Thanks for using this bashtop auto-install script, made by PapasFilms!

echo Please be patient

echo ...

cd

git clone https://github.com/aristocratos/bashtop.git

cd bashtop

sudo make install

cd

cd /home/$USER/bashtop-application.desktop-file-with-icon

sudo cp /home/$USER/bashtop-application.desktop-file-with-icon/bashtop.desktop /usr/share/applications

sudo cp /home/$USER/bashtop-application.desktop-file-with-icon/bashtop.ico /usr/local/bin



echo Done! Enjoy!
